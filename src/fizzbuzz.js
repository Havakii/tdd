const fizzbuzz = (num) => {
    return num % 7 == 0 && num % 5 == 0 ? "fizzbuzz"
            : num % 5 == 0 ? "buzz" 
            : num % 7 == 0 ? "fizz"
            : num == null || num == "" ? "Error!"
            :"";
}

exports.fizzbuzz = fizzbuzz;