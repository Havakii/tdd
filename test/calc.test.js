const expect = require("chai").expect;
const { calc } = require("../src/calc");

describe("Calc", () => {
  describe("Addition", () => {
    it('should sum 2 numbers', () => {
        expect(calc.add(1, 1)).to.equal(2)
    })
  });
});
